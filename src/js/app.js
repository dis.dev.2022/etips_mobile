"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import {WebpMachine} from "webp-hero"
import HystModal from 'hystmodal';

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// IE Webp Support
const webpMachine = new WebpMachine();
webpMachine.polyfillDocument();

// Modal
const myModal = new HystModal({
    linkAttributeName: 'data-hystmodal',
});


document.addEventListener('click', Details);

function Details(event) {
    if (event.target.closest('[data-detail]')) {
        event.target.closest('[data-detail]').classList.toggle('is-selected')

        document.querySelector('[data-panel-feedback]').classList.toggle('is-selected')
        document.querySelector('[data-panel-feedback]').disabled = false
    }
}

document.addEventListener('click', Rating)

function Rating(event) {
    if (event.target.closest('[data-rating-item]')) {
        const rating = event.target.closest('[data-rating]')
        const ratingArray = rating.querySelectorAll('[data-rating-item]')
        const ratingValue = event.target.closest('[data-rating-item]').dataset.ratingItem;
        ratingArray.forEach(elem => {
            elem.classList.remove('is-checked');
        });
        event.target.closest('[data-rating-item]').classList.add('is-checked')
        document.querySelector('[data-panel-feedback]').disabled = false

        if (ratingValue > 3) {
            document.querySelector('[data-review]').classList.add('open')
        }
        else {
            document.querySelector('[data-review]').classList.remove('open')
        }
    }
}


document.addEventListener('click', Suggest)

function Suggest(event) {
    if (event.target.closest('[data-suggest]')) {
        const suggests = event.target.closest('[data-suggests]')
        const suggestArray = suggests.querySelectorAll('[data-suggest]')
        const suggestValue = event.target.closest('[data-suggest]').dataset.suggest;
        suggestArray.forEach(elem => {
            elem.classList.remove('is-selected');
        });
        event.target.closest('[data-suggest]').classList.add('is-selected')
        document.querySelector('[data-amount]').value = suggestValue
        document.querySelector('[data-panel]').classList.add('panel--pay')
    }
}

document.addEventListener('click', Switcher)
function Switcher(event) {
    if (event.target.closest('[data-user]')) {
        const users = event.target.closest('[data-users]')
        const usersArray = users.querySelectorAll('[data-user]')
        const user = event.target.closest('[data-user]')
        const userName = user.dataset.user
        const userText = user.dataset.userText
        const userAvatar = user.dataset.userAvatar

        usersArray.forEach(elem => {
            elem.classList.remove('active');
        });
        user.classList.add('active')

        document.querySelector('[data-person-name]').innerHTML = userName
        document.querySelector('[data-person-text]').innerHTML = userText
        document.querySelector('[data-person-avatar]').src = userAvatar
        myModal.close()

        console.log(userName + ', ' + userText + ', ' + userAvatar)
    }
}

document.querySelector('[data-amount]').oninput = function() {
    document.querySelectorAll('[data-suggest]').forEach(elem => {
        elem.classList.remove('is-selected');
    });

    if (document.querySelector('[data-amount]').value) {
        document.querySelector('[data-panel]').classList.add('panel--pay')
    }
    else {
        document.querySelector('[data-panel]').classList.remove('panel--pay')
    }
};

let input = document.querySelector('[data-amount]');


function setInputFilter(textbox, inputFilter, errMsg) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop", "focusout"].forEach(function(event) {
        textbox.addEventListener(event, function(e) {
            if (inputFilter(this.value)) {
                // Accepted value
                if (["keydown","mousedown","focusout"].indexOf(e.type) >= 0){
                    this.classList.remove("input-error");
                    this.setCustomValidity("");
                }
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                // Rejected value - restore the previous one
                this.classList.add("input-error");
                this.setCustomValidity(errMsg);
                this.reportValidity();
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
                // Rejected value - nothing to restore
                this.value = "";
            }
        });
    });
}

setInputFilter(input, function(value) {
    return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
}, "Only digits and '.' are allowed");



